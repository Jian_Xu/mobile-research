package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;
import java.util.UUID;

public class HearstSensorActivity extends Activity {


    private static final String TAG = HearstSensorActivity.class.getCanonicalName();
    private static final int MIN_UINT = 0;
    private static final int MAX_UINT8 = (int) Math.pow(2, 8) - 1;
    private static final int MAX_UINT16 = (int) Math.pow(2, 16) - 1;

    /**
     * See <a href="https://developer.bluetooth.org/gatt/services/Pages/ServiceViewer.aspx?u=org.bluetooth.service.heart_rate.xml">
     * Heart Rate Service</a>
     */
    private static final UUID HEART_RATE_SERVICE_UUID = UUID
            .fromString("0000180D-0000-1000-8000-00805f9b34fb");

    /**
     * See <a href="https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml">
     * Heart Rate Measurement</a>
     */
    private static final UUID HEART_RATE_MEASUREMENT_UUID = UUID
            .fromString("00002A37-0000-1000-8000-00805f9b34fb");
    private static final int HEART_RATE_MEASUREMENT_VALUE_FORMAT = BluetoothGattCharacteristic.FORMAT_UINT8;
    private static final int INITIAL_HEART_RATE_MEASUREMENT_VALUE = 60;
    private static final int EXPENDED_ENERGY_FORMAT = BluetoothGattCharacteristic.FORMAT_UINT16;
    private static final int INITIAL_EXPENDED_ENERGY = 0;
    private static final String HEART_RATE_MEASUREMENT_DESCRIPTION = "Used to send a heart rate " +
            "measurement";

    /**
     * See <a href="https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.body_sensor_location.xml">
     * Body Sensor Location</a>
     */
    private static final UUID BODY_SENSOR_LOCATION_UUID = UUID
            .fromString("00002A38-0000-1000-8000-00805f9b34fb");
    private static final int LOCATION_OTHER = 0;

    /**
     * See <a href="https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_control_point.xml">
     * Heart Rate Control Point</a>
     */
    private static final UUID HEART_RATE_CONTROL_POINT_UUID = UUID
            .fromString("00002A39-0000-1000-8000-00805f9b34fb");

    private BluetoothGattService mHeartRateService;
    private BluetoothGattCharacteristic mHeartRateMeasurementCharacteristic;
    private BluetoothGattCharacteristic mBodySensorLocationCharacteristic;
    private BluetoothGattCharacteristic mHeartRateControlPoint;

    private static final UUID CHARACTERISTIC_USER_DESCRIPTION_UUID = UUID
            .fromString("00002901-0000-1000-8000-00805f9b34fb");
    private static final UUID CLIENT_CHARACTERISTIC_CONFIGURATION_UUID = UUID
            .fromString("00002902-0000-1000-8000-00805f9b34fb");

    public HearstSensorActivity(){
        mHeartRateMeasurementCharacteristic =
                new BluetoothGattCharacteristic(HEART_RATE_MEASUREMENT_UUID,
                        BluetoothGattCharacteristic.PROPERTY_NOTIFY,
            /* No permissions */ 0);

        BluetoothGattDescriptor descriptor = new BluetoothGattDescriptor(
                CLIENT_CHARACTERISTIC_CONFIGURATION_UUID,
                (BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE));
        descriptor.setValue(new byte[]{0, 0});

        mHeartRateMeasurementCharacteristic.addDescriptor(descriptor);

        descriptor = new BluetoothGattDescriptor(
                CHARACTERISTIC_USER_DESCRIPTION_UUID,
                (BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE));
                   descriptor.setValue(HEART_RATE_MEASUREMENT_DESCRIPTION.getBytes());
        mHeartRateMeasurementCharacteristic.addDescriptor(descriptor);

        mBodySensorLocationCharacteristic =
                new BluetoothGattCharacteristic(BODY_SENSOR_LOCATION_UUID,
                        BluetoothGattCharacteristic.PROPERTY_READ,
                        BluetoothGattCharacteristic.PERMISSION_READ);

        mHeartRateControlPoint =
                new BluetoothGattCharacteristic(HEART_RATE_CONTROL_POINT_UUID,
                        BluetoothGattCharacteristic.PROPERTY_WRITE,
                        BluetoothGattCharacteristic.PERMISSION_WRITE);

        mHeartRateService = new BluetoothGattService(HEART_RATE_SERVICE_UUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        mHeartRateService.addCharacteristic(mHeartRateMeasurementCharacteristic);
        mHeartRateService.addCharacteristic(mBodySensorLocationCharacteristic);
        mHeartRateService.addCharacteristic(mHeartRateControlPoint);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hearst_sensor);


        setHeartRateMeasurementValue(INITIAL_HEART_RATE_MEASUREMENT_VALUE,
                INITIAL_EXPENDED_ENERGY);
        setBodySensorLocationValue(LOCATION_OTHER);


    }

    private void setHeartRateMeasurementValue(int heartRateMeasurementValue, int expendedEnergy) {

        Log.d(TAG, Arrays.toString(mHeartRateMeasurementCharacteristic.getValue()));
    /* Set the org.bluetooth.characteristic.heart_rate_measurement
     * characteristic to a byte array of size 4 so
     * we can use setValue(value, format, offset);
     *
     * Flags (8bit) + Heart Rate Measurement Value (uint8) + Energy Expended (uint16) = 4 bytes
     *
     * Flags = 1 << 3:
     *   Heart Rate Format (0) -> UINT8
     *   Sensor Contact Status (00) -> Not Supported
     *   Energy Expended (1) -> Field Present
     *   RR-Interval (0) -> Field not pressent
     *   Unused (000)
     */
        mHeartRateMeasurementCharacteristic.setValue(new byte[]{0b00001000, 0, 0, 0});
        // Characteristic Value: [flags, 0, 0, 0]
        mHeartRateMeasurementCharacteristic.setValue(heartRateMeasurementValue,
                HEART_RATE_MEASUREMENT_VALUE_FORMAT,
        /* offset */ 1);
        // Characteristic Value: [flags, heart rate value, 0, 0]
   ///PP///     mEditTextHeartRateMeasurement.setText(Integer.toString(heartRateMeasurementValue));
        mHeartRateMeasurementCharacteristic.setValue(expendedEnergy,
                EXPENDED_ENERGY_FORMAT,
        /* offset */ 2);
        // Characteristic Value: [flags, heart rate value, energy expended (LSB), energy expended (MSB)]
        ///PP///     mEditTextEnergyExpended.setText(Integer.toString(expendedEnergy));
    }

    private void setBodySensorLocationValue(int location) {
        mBodySensorLocationCharacteristic.setValue(new byte[]{(byte) location});
        ///PP/// mSpinnerBodySensorLocation.setSelection(location);
    }



}

This repository contains documents relating BLE study made by Praveen Kumar Saminathan (110739913).

Study docs based on various BLE sources:
1) BLE Advertisment and Service Discovery.pdf
2) BLE_study.pdf


BLE estimation and analysis related docs:
1) BLE Snoop Log file : btsnoop_hci_BLE.log
2) Observation and results related to the experiments: Result.xlsx
3) Study report for througput for BLE: Througput estimation.pdf

Android sourcecode for BLE
1) BluetoothLeGatt
	- Implementation for Server(Pheripheral device) and Client (SmartPhone) is implemented a same project.
	- Client :
		- Scans for new devices
		- Connects with any device
		- Reads Service & character from the Server device.
		- Reads data from server device and data change notification.
		
	- InOrder to calcualte the througout, data are continously generated from the Server device and notified to the Client Device.
	- Number of Connection Intervals, Max sixe data packets are analysed using BT snoop log.
	- Futther implementation details are available in Report.
		